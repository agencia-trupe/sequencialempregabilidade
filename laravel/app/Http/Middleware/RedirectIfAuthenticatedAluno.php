<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticatedAluno
{
    public function handle($request, Closure $next)
    {
        if (Auth::guard('aluno')->check()) {
            return redirect('vagas');
        }

        return $next($request);
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\UsuarioPainel;
use Illuminate\Http\Request;

class AuthAlunoController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest.aluno', ['except' => 'logout']);
    }

    protected function login(Request $request)
    {
        $user = UsuarioPainel::where([
            'email' => $request->email,
            'senha' => md5($request->password)
        ])->first();

        if ($user) {
            auth('aluno')->login($user);
            return redirect()->route('vagas');
        } else {
            return redirect()->route('home')->withInput()->with('error', true);
        }
    }

    protected function logout()
    {
        auth('aluno')->logout();
        return redirect()->route('home');
    }
}

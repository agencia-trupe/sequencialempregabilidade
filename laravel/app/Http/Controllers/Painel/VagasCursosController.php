<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\VagasCursosRequest;
use App\Http\Controllers\Controller;

use App\Models\VagaCurso;

class VagasCursosController extends Controller
{
    public function index()
    {
        $cursos = VagaCurso::ordenados()->get();

        return view('painel.vagas.cursos.index', compact('cursos'));
    }

    public function create()
    {
        return view('painel.vagas.cursos.create');
    }

    public function store(VagascursosRequest $request)
    {
        try {

            $input = $request->all();

            VagaCurso::create($input);
            return redirect()->route('painel.vagas.cursos.index')->with('success', 'Curso adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar curso: '.$e->getMessage()]);

        }
    }

    public function edit(VagaCurso $curso)
    {
        return view('painel.vagas.cursos.edit', compact('curso'));
    }

    public function update(VagascursosRequest $request, VagaCurso $curso)
    {
        try {

            $input = $request->all();

            $curso->update($input);
            return redirect()->route('painel.vagas.cursos.index')->with('success', 'Curso alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar curso: '.$e->getMessage()]);

        }
    }

    public function destroy(VagaCurso $curso)
    {
        try {

            $curso->delete();
            return redirect()->route('painel.vagas.cursos.index')->with('success', 'Curso excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir curso: '.$e->getMessage()]);

        }
    }
}

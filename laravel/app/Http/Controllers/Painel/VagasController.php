<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\VagasRequest;
use App\Http\Controllers\Controller;

use App\Models\Vaga;
use App\Models\VagaCurso;
use Carbon\Carbon;

class VagasController extends Controller
{
    private $cursos;

    public function __construct()
    {
        $this->cursos = VagaCurso::ordenados()->lists('titulo', 'id');
    }

    public function index()
    {
        $registros = Vaga::orderBy('data_de_cadastro', 'DESC')->paginate(20);

        return view('painel.vagas.index', compact('registros'));
    }

    public function create()
    {
        $cursos = $this->cursos;

        return view('painel.vagas.create', compact('cursos'));
    }

    public function store(VagasRequest $request)
    {
        try {

            $input = $request->except('cursos');
            $cursos  = ($request->get('cursos') ?: []);

            $input['nivel'] = implode(',', $request->get('nivel'));

            if (!$input['data_de_expiracao']) {
                $input['data_de_expiracao'] = null;
            }

            $input['cadastrada_em'] = Carbon::now();
            $input['cadastrada_por'] = auth()->user()->id;

            $registro = Vaga::create($input);
            $registro->cursos()->sync($cursos);

            return redirect()->route('painel.vagas.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Vaga $registro)
    {
        $cursos = $this->cursos;

        return view('painel.vagas.edit', compact('registro', 'cursos'));
    }

    public function update(VagasRequest $request, Vaga $registro)
    {
        try {

            $input = $request->except('cursos');

            $input['nivel'] = implode(',', $request->get('nivel'));

            if (!$input['data_de_expiracao']) {
                $input['data_de_expiracao'] = null;
            }

            if (!$input['preenchida']) {
                $input['preenchida'] = null;
                $input['preenchida_em'] = null;
                $input['preenchida_por'] = null;
            } else if ($input['preenchida'] !== $registro->preenchida) {
                $input['preenchida_em'] = Carbon::now();
                $input['preenchida_por'] = auth()->user()->id;
            }

            $registro->update($input);

            $cursos = ($request->get('cursos') ?: []);
            $registro->cursos()->sync($cursos);

            return redirect()->route('painel.vagas.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Vaga $registro)
    {
        try {

            $registro->cursos()->detach();
            $registro->delete();

            return redirect()->route('painel.vagas.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}

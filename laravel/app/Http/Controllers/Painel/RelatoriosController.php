<?php

namespace App\Http\Controllers\Painel;

use App\Helpers\Tools;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\Vaga;
use App\Models\VagaCurso;
use App\Models\VagaVisualizacao;
use App\Models\TermoBuscado;
use App\Models\CursoBuscado;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class RelatoriosController extends Controller
{
    public function vagas(Request $request)
    {
        $cursos = VagaCurso::ordenados()->lists('titulo', 'id');
        $vagas  = new Vaga;

        if ($curso = $request->get('curso')) {
            $vagas = $vagas->whereHas('cursos', function($query) use ($curso) {
                $query->where('vaga_curso.vaga_curso_id', (int) $curso);
            });
        }

        $total  = $vagas->count();
        $ativas = $vagas->emExibicao()->count();

        return view('painel.relatorios.vagas', compact('cursos', 'total', 'ativas'));
    }

    public function cliques(Request $request)
    {
        $cursos  = VagaCurso::ordenados()->lists('titulo', 'id');
        $cliques = $this->filtraPeriodo($request, new VagaVisualizacao);

        if ($termo = $request->get('termo')) {
            $cliques = $cliques->whereHas('vaga', function($query) use ($termo) {
                $query->where('titulo', 'LIKE', "%$termo%");
            });
        }

        if ($curso = $request->get('curso')) {
            $cliques = $cliques->whereHas('vaga.cursos', function($query) use ($curso) {
                $query->where('vaga_curso.vaga_curso_id', (int) $curso);
            });
        }

        $cliques = $cliques
            ->with('vaga')
            ->groupBy('vaga_id')
            ->select('vaga_id', DB::raw('count(*) as total'))
            ->orderBy('total', 'DESC')
            ->paginate(20);

        return view('painel.relatorios.cliques', compact('cursos', 'cliques'));
    }

    public function termos(Request $request)
    {
        $termos = $this->filtraPeriodo($request, new TermoBuscado);
        $termos = $termos
            ->groupBy('termo')
            ->select('termo', DB::raw('count(*) as total'))
            ->orderBy('total', 'DESC')
            ->orderBy('termo', 'ASC')
            ->paginate(20);

        return view('painel.relatorios.termos', compact('termos'));
    }

    public function cursos(Request $request)
    {
        $cursos = $this->filtraPeriodo($request, new CursoBuscado);
        $cursos = $cursos
            ->with('curso')
            ->groupBy('curso_id')
            ->select('curso_id', DB::raw('count(*) as total'))
            ->orderBy('total', 'DESC')
            ->get();

        return view('painel.relatorios.cursos', compact('cursos'));
    }

    public function preenchidas(Request $request)
    {
        if ($filter = $request->get('por')) {
            if (!in_array($filter, array_keys(Tools::vagaStatus()))) {
                abort('404');
            }

            $vagas = Vaga::where('preenchida', $filter)
                ->orderBy('data_de_cadastro', 'DESC')
                ->paginate(20);

            return view('painel.relatorios.preenchidas.list', compact('vagas'));
        }

        $contagem = Vaga::whereNotNull('preenchida')
            ->groupBy('preenchida')
            ->select('preenchida', DB::raw('count(*) as total'))
            ->lists('total', 'preenchida');

        return view('painel.relatorios.preenchidas.contagem', compact('contagem'));
    }

    protected function filtraPeriodo(Request $request, $registros)
    {
        $inicio = null;
        $final  = null;

        try {
            if ($request->get('data_inicio') && Carbon::createFromFormat('d/m/Y', $request->get('data_inicio'))) {
                $inicio = Carbon::createFromFormat('d/m/Y', $request->get('data_inicio'))->format('Y-m-d');
            }
            if ($request->get('data_final') && Carbon::createFromFormat('d/m/Y', $request->get('data_final'))) {
                $final = Carbon::createFromFormat('d/m/Y', $request->get('data_final'))->format('Y-m-d');
            }
        } catch (\Exception $e) {
        }

        if ($inicio) {
            $registros = $registros->whereDate('created_at', '>=', $inicio);
        }
        if ($final) {
            $registros = $registros->whereDate('created_at', '<=', $final);
        }

        return $registros;
    }
}

<?php

namespace App\Http\Controllers;

use App\Helpers\Tools;
use Illuminate\Http\Request;

use App\Models\Vaga;
use App\Models\VagaCurso;
use App\Models\VagaVisualizacao;
use App\Models\CursoBuscado;
use App\Models\TermoBuscado;

class VagasController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('page') && !$request->ajax()) {
            return redirect()->route('vagas', $request->except('page'));
        }

        $aluno  = auth('aluno')->user();
        $niveis = Vaga::niveis();
        $cursos = VagaCurso::ordenados()->get();

        $vagas = Vaga::with('cursos')->emExibicao()->orderBy('data_de_cadastro', 'DESC');

        $vagas = $vagas->where(function($query) use ($request) {
            foreach(Vaga::niveis() as $slug => $nivel) {
                if ($request->get($slug)) {
                    $query->orWhere('nivel', 'LIKE', "%$nivel%");
                }
            }
        });

        if ($curso = $request->get('curso')) {
            $vagas = $vagas->whereHas('cursos', function($query) use ($curso) {
                $query->where('slug', $curso);
            });


            $cursoObj = $cursos->where('slug', $curso)->first();
            if (!request()->has('page') && $cursoObj) {
                CursoBuscado::create([
                    'curso_id'      => $cursoObj->id,
                    'usuario_id'    => $aluno->id,
                    'usuario_email' => $aluno->email
                ]);
            }
        }

        if ($termo = $request->get('termo')) {
            $vagas = $vagas
                ->where('titulo', 'LIKE', "%$termo%")
                ->orWhere('descricao', 'LIKE', "%$termo%");

            if (!request()->has('page')) {
                TermoBuscado::create([
                    'termo'         => $termo,
                    'usuario_id'    => $aluno->id,
                    'usuario_email' => $aluno->email
                ]);
            }
        }

        $vagas = $vagas->paginate(6);

        if ($request->ajax()) {
            return response()->json([
                'body' => view('frontend._vagas', compact('vagas'))->render(),
                'end'  => !$vagas->hasMorePages()
            ]);
        }

        return view('frontend.vagas', compact('niveis', 'cursos', 'vagas'));
    }

    public function show(Vaga $vaga, $tipo) {
        $aluno = auth('aluno')->user();

        $text = null;
        $link = null;

        if ($tipo == 'telefone' && $vaga->telefone) {
            $text = $vaga->telefone;
        } else if ($tipo == 'e_mail' && $vaga->e_mail) {
            $text = $vaga->e_mail;
            $link = 'mailto:'.$vaga->e_mail;
        } else if ($tipo == 'site' && $vaga->site) {
            $text = $vaga->site;
            $link = Tools::parseLink($vaga->site);
        }

        VagaVisualizacao::firstOrCreate([
            'vaga_id'       => $vaga->id,
            'usuario_id'    => $aluno->id,
            'usuario_email' => $aluno->email
        ]);

        return response()->json(compact('text', 'link'));
    }
}

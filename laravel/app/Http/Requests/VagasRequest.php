<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Helpers\Tools;

class VagasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'data_de_cadastro' => 'required',
            'cursos' => 'required',
            'titulo' => 'required',
            'descricao' => 'required',
            'empresa' => 'required',
            'empresa_dados' => '',
            'empresa_amiga' => '',
            'ocultar_empresa' => '',
            'local' => '',
            'horario' => '',
            'salario' => '',
            'beneficios' => '',
            'nivel' => 'required|array',
            'ativa' => '',
            'data_de_expiracao' => '',
            'telefone' => 'required_without_all:e_mail,site',
            'e_mail' => 'required_without_all:telefone,site|email',
            'site' => 'required_without_all:e_mail,telefone',
            'preenchida' => 'in:'.implode(',', array_keys(Tools::vagaStatus())),
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}

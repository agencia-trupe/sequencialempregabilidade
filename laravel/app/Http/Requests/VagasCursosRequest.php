<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class VagasCursosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titulo' => 'required'
        ];
    }
}

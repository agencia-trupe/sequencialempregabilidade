<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home')->middleware('guest.aluno');

    Route::group(['middleware' => 'auth.aluno'], function() {
        Route::get('vagas', 'VagasController@index')->name('vagas');
        Route::get('vagas/{vagas}/{tipo}', 'VagasController@show')->name('vagas.info');
    });

    Route::group(['namespace' => 'Auth'], function() {
        Route::post('login', 'AuthAlunoController@login')->name('aluno.login');
        Route::get('logout', 'AuthAlunoController@logout')->name('aluno.logout');
    });

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('vagas/cursos', 'VagasCursosController', ['parameters' => ['cursos' => 'cursos_vagas']]);
        Route::resource('vagas', 'VagasController');

        Route::get('relatorios/vagas', 'RelatoriosController@vagas')->name('painel.relatorios.vagas');
        Route::get('relatorios/cliques', 'RelatoriosController@cliques')->name('painel.relatorios.cliques');
        Route::get('relatorios/termos', 'RelatoriosController@termos')->name('painel.relatorios.termos');
        Route::get('relatorios/cursos', 'RelatoriosController@cursos')->name('painel.relatorios.cursos');
        Route::get('relatorios/vagas-preenchidas', 'RelatoriosController@preenchidas')->name('painel.relatorios.preenchidas');

		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

        // Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        // Route::resource('contato/recebidos', 'ContatosRecebidosController');
        // Route::resource('contato', 'ContatoController');

        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});

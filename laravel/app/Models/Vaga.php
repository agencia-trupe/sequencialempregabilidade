<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Vaga extends Model
{
    protected $table = 'vagas';

    protected $guarded = ['id'];

    protected $dates = [
        'data_de_cadastro',
        'data_de_expiracao',
        'cadastrada_em',
        'preenchida_em'
    ];

    public function cursos()
    {
        return $this->belongsToMany('App\Models\VagaCurso', 'vaga_curso', 'vaga_id', 'vaga_curso_id')->ordenados();
    }

    public function setDataDeCadastroAttribute($date)
    {
        if ($date) {
            $this->attributes['data_de_cadastro'] = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
        } else {
            $this->attributes['data_de_cadastro'] = null;
        }
    }

    public function getDataCadastroAttribute()
    {
        return $this->data_de_cadastro->format('d/m/Y');
    }

    public function setDataDeExpiracaoAttribute($date)
    {
        if ($date) {
            $this->attributes['data_de_expiracao'] = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
        } else {
            $this->attributes['data_de_expiracao'] = null;
        }
    }

    public function getDataFormatadaAttribute()
    {
        return $this->data_de_expiracao
            ? $this->data_de_expiracao->format('d/m/Y')
            : null;
    }

    public static function niveis()
    {
        return [
            'tecnico'   => 'Nível Técnico',
            'graduacao' => 'Graduação',
        ];
    }

    public function getEmExibicaoAttribute()
    {
        if ($this->data_de_expiracao && Carbon::now()->gt($this->data_de_expiracao)) {
            return false;
        }

        return $this->ativa && !$this->preenchida;
    }

    public function scopeEmExibicao($query)
    {
        return $query
            ->where('ativa', 1)
            ->where(function($query) {
                $query
                    ->whereNull('data_de_expiracao')
                    ->orWhereDate('data_de_expiracao', '>', Carbon::now());
            });
    }

    public function cadastradaPor()
    {
        return $this->belongsTo(User::class, 'cadastrada_por');
    }

    public function preenchidaPor()
    {
        return $this->belongsTo(User::class, 'preenchida_por');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CursoBuscado extends Model
{
    protected $table = 'cursos_buscados';

    protected $guarded = ['id'];

    public function curso()
    {
        return $this->belongsTo(VagaCurso::class, 'curso_id');
    }
}

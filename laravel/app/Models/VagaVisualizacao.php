<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VagaVisualizacao extends Model
{
    protected $table = 'vagas_visualizacoes';

    protected $guarded = ['id'];

    public function vaga()
    {
        return $this->belongsTo(Vaga::class, 'vaga_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TermoBuscado extends Model
{
    protected $table = 'termos_buscados';

    protected $guarded = ['id'];
}

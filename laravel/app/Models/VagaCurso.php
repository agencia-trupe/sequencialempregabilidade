<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Support\Facades\Cache;

class VagaCurso extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'vagas_cursos';

    protected $guarded = ['id'];

    public static function boot()
    {
        parent::boot();

        self::created(function() {
            Cache::forget('cursos_count');
            return static::cachedCount();
        });

        self::deleted(function() {
            Cache::forget('cursos_count');
            return static::cachedCount();
        });
    }

    public static function cachedCount()
    {
        return Cache::rememberForever('cursos_count', function() {
            return static::count();
        });
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function vagas()
    {
        return $this->belongsToMany('App\Models\Vaga', 'vaga_curso', 'vaga_curso_id', 'vaga_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class UsuarioPainel extends Model implements AuthenticatableContract
{
    use Authenticatable;

    protected $connection = 'portal_alunos';
    protected $table;

    public function __construct()
    {
        $this->table = env('DB_PORTAL_USERS_TABLE');
    }

    public function setAttribute($key, $value)
    {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();

        if (!$isRememberTokenAttribute) {
            parent::setAttribute($key, $value);
        }
    }
}

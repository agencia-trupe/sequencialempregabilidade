<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVagasTable extends Migration
{
    public function up()
    {
        Schema::create('vagas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('data_de_cadastro');
            $table->string('titulo');
            $table->text('descricao');
            $table->string('empresa');
            $table->text('empresa_dados');
            $table->boolean('empresa_amiga');
            $table->boolean('ocultar_empresa');
            $table->string('local');
            $table->string('horario');
            $table->string('salario');
            $table->string('beneficios');
            $table->string('nivel');
            $table->boolean('ativa');
            $table->string('data_de_expiracao')->nullable();
            $table->string('telefone');
            $table->string('e_mail');
            $table->string('site');
            $table->string('preenchida')->nullable();
            $table->dateTime('preenchida_em')->nullable();
            $table->integer('preenchida_por')->unsigned()->nullable();
            $table->foreign('preenchida_por')->references('id')->on('users');
            $table->dateTime('cadastrada_em')->nullable();
            $table->integer('cadastrada_por')->unsigned()->nullable();
            $table->foreign('cadastrada_por')->references('id')->on('users');
            $table->timestamps();
        });

        Schema::create('vagas_cursos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('vaga_curso', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vaga_id')->unsigned();
            $table->integer('vaga_curso_id')->unsigned();
            $table->timestamps();
            $table->foreign('vaga_id')->references('id')->on('vagas')->onDelete('cascade');
            $table->foreign('vaga_curso_id')->references('id')->on('vagas_cursos')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('vaga_curso');
        Schema::drop('vagas_cursos');
        Schema::drop('vagas');
    }
}

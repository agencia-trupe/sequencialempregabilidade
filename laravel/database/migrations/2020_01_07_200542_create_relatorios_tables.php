<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelatoriosTables extends Migration
{
    public function up()
    {
        Schema::create('vagas_visualizacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vaga_id')->unsigned();
            $table->foreign('vaga_id')->references('id')->on('vagas')->onDelete('cascade');
            $table->integer('usuario_id')->unsigned();
            $table->string('usuario_email');
            $table->timestamps();
        });

        Schema::create('cursos_buscados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('curso_id')->unsigned();
            $table->foreign('curso_id')->references('id')->on('vagas_cursos')->onDelete('cascade');
            $table->integer('usuario_id')->unsigned();
            $table->string('usuario_email');
            $table->timestamps();
        });

        Schema::create('termos_buscados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('termo');
            $table->integer('usuario_id')->unsigned();
            $table->string('usuario_email');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('termos_buscados');
        Schema::drop('cursos_buscados');
        Schema::drop('vagas_visualizacoes');
    }
}

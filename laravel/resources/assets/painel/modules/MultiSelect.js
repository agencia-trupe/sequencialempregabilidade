export default function MultiSelect() {
    $('.multi-select').multiselect({
        nonSelectedText: 'Selecione',
        allSelectedText: 'Todos',
        nSelectedText: ' selecionados',
        includeSelectAllOption: true,
        selectAllText: 'Selecionar todos',
        numberDisplayed: 4,
        buttonWidth: '100%',
        maxHeight: 250,
    });
}

import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

const macy = window.Macy({
    container: '.vagas-container',
    margin: 40,
    columns: 2,
    breakAt: {
        1200: {
            margin: 30,
        },
        900: {
            columns: 1,
            margin: 24,
        },
    },
});

$('.handle-paginate').click(function handlePaginate() {
    const $btn = $(this);
    const $container = $('.vagas-container');

    const { url, page } = $btn.data();

    if ($btn.hasClass('loading')) return;

    $btn.addClass('loading');

    $.get(url, { page: page + 1 }, (data) => {
        $container.append(data.body);
        macy.recalculate();
        $btn.data('page', page + 1);
        data.end && $btn.remove();
    }).fail(() => {
        alert('Ocorreu um erro ao carregar mais resultados.');
    }).always(() => {
        $btn.removeClass('loading');
    });
});

$('.vagas-container').on('click', '.handle-contato', function handleContato(event) {
    const $handle = $(this);

    if ($handle.hasClass('loaded') && $handle.attr('href') !== '#') {
        return;
    }

    event.preventDefault();

    if ($handle.hasClass('loading') || $handle.hasClass('loaded')) return;
    $handle.addClass('loading');

    $.get($handle.data('url'), (data) => {
        data.text && $handle.addClass('loaded').find('span').text(data.text);
        data.link && $handle.attr('href', data.link);
    }).fail(() => {
        alert('Ocorreu um erro ao carregar os dados.');
    }).always(() => {
        $handle.removeClass('loading');
    });
});

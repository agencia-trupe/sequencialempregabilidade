@extends('frontend.common.template')

@section('content')

    <div class="main main-interna">
        <div class="center">
            <div class="main--header">
                <img src="{{ asset('assets/img/layout/marca-centralempregabilidade.png') }}" alt="Central de Empregabilidade Sequencial">
                <div class="links">
                    <a href="{{ route('aluno.logout') }}">
                        Olá {{ explode(' ', auth('aluno')->user()->nome)[0] }}
                        <span class="logout">SAIR</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="busca">
        <div class="center">
            <form action="{{ route('vagas') }}" method="GET">
                <div class="niveis">
                    @foreach($niveis as $slug => $nivel)
                    <label>
                        <input type="checkbox" name="{{ $slug }}" value="1" @if(request($slug)) checked @endif>
                        <span>{{ $nivel }}</span>
                    </label>
                    @endforeach
                </div>
                <select name="curso">
                    <option value="">Selecione o curso (TODOS)</option>
                    @foreach($cursos as $curso)
                    <option value="{{ $curso->slug }}" @if(request('curso') == $curso->slug) selected @endif>{{ $curso->titulo }}</option>
                    @endforeach
                </select>
                <input type="text" name="termo" value="{{ request('termo') }}" placeholder="Palavra-chave (título da vaga)">
                <button>Buscar<br> Vagas</button>
            </form>
        </div>
    </div>

    <div class="vagas">
        <div class="center">
            @if(!count($vagas))
                <h2>Nenhuma vaga encontrada</h2>
            @else
                <h2>Vagas em destaque</h2>

                <div class="vagas-container">
                    @include('frontend._vagas', compact('vagas'))
                </div>

                @if($vagas->hasMorePages())
                    <button data-url="{{ request()->fullUrl() }}" data-page="1" class="handle-paginate">
                        ver mais
                        <span></span>
                    </button>
                @endif
            @endif
        </div>
    </div>

@endsection

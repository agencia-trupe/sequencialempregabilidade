@foreach($vagas as $vaga)
    <div class="vaga">
        <div class="vaga-cursos">
            <span>PARA PROFISSIONAIS DOS CURSOS DE:</span>
            <p>
                @if(count($vaga->cursos) == \App\Models\VagaCurso::cachedCount())
                <span>Todos os cursos</span>
                @else
                    @foreach($vaga->cursos as $curso)
                    <span>{{ $curso->titulo }}</span>
                    @endforeach
                @endif
            </p>
        </div>
        <div class="vaga-conteudo">
            <h3>{{ $vaga->titulo }}</h3>
            @if(!$vaga->ocultar_empresa) <h4>{{ $vaga->empresa }}</h4> @endif
            <div class="descricao">{!! $vaga->descricao !!}</div>

            <div class="info">
                @foreach([
                    'local'      => 'Local',
                    'horario'    => 'Horário',
                    'salario'    => 'Salário',
                    'beneficios' => 'Benefícios'
                ] as $info => $title)
                    @if($vaga->{$info})
                    <div>
                        <span class="{{ $info }}">{{ $title }}</span>
                        <span>{{ $vaga->{$info} }}</span>
                    </div>
                    @endif
                @endforeach
            </div>

            <div class="contatos">
                <p>Como se candidatar para esta vaga</p>

                <div class="botoes">
                    @if($vaga->telefone)
                    <a
                        href="#"
                        class="handle-contato contato-telefone"
                        data-url="{{ route('vagas.info', [$vaga->id, 'telefone']) }}"
                    >
                        LIGUE PARA MAIS INFORMAÇÕES
                        <span>VER TELEFONE &raquo;</span>
                    </a>
                    @endif

                    @if($vaga->e_mail)
                    <a
                        href="#"
                        class="handle-contato contato-email"
                        data-url="{{ route('vagas.info', [$vaga->id, 'e_mail']) }}"
                    >
                        ENVIE UM E-MAIL COM SEU CURRÍCULO
                        <span>VER E-MAIL &raquo;</span>
                    </a>
                    @endif

                    @if($vaga->site)
                    <a
                        href="#"
                        target="_blank"
                        class="handle-contato contato-site"
                        data-url="{{ route('vagas.info', [$vaga->id, 'site']) }}"
                    >
                        ACESSE O SITE PARA ENVIAR O CURRÍCULO
                        <span>VER ENDEREÇO DO SITE &raquo;</span>
                    </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endforeach

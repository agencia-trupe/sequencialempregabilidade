    <footer>
        <div class="center">
            <div>
                <p>Você deve ser aluno com login ativo do Portal do Aluno Sequencial ou ex-aluno com cadastro aprovado pela Sequencial para acessar as funcionalidades desta plataforma.</p>
                <p>Para dúvidas ou informações acesse o contato da sua unidade.</p>
            </div>
            <div>
                <img src="{{ asset('assets/img/layout/marca-centralempregabilidade-rodape.png') }}" alt="">
            </div>
        </div>
    </footer>

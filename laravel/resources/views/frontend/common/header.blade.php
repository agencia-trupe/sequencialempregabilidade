    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ config('app.name') }}</a>
            <nav>
                @include('frontend.common.nav')
            </nav>
        </div>
    </header>

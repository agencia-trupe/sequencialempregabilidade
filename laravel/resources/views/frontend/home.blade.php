@extends('frontend.common.template')

@section('content')

    @if(session('error'))
    <div class="erro-login">
        <div class="center">
            E-mail ou senha inválidos.
        </div>
    </div>
    @endif

    <div class="main">
        <div class="center">
            <div class="main--header">
                <img src="{{ asset('assets/img/layout/marca-centralempregabilidade.png') }}" alt="Central de Empregabilidade Sequencial">
                <div class="links">
                    <a href="http://faculdadesequencial.com.br" target="_blank">
                        Visite o site da
                        <strong>Faculdade Sequencial</strong>
                    </a>
                    <a href="http://escolasequencial.com.br" target="_blank">
                        Visite o site da
                        <strong>Escola Técnica Sequencial</strong>
                    </a>
                </div>
            </div>

            <div class="main--content">
                <div class="left">
                    <h2>
                        A Central de Vagas de Empregos para Alunos e Ex-alunos
                        da Faculdade e Escola Técnica Sequencial
                    </h2>
                    <div class="login">
                        <p>Utilize os mesmos dados de acesso do Portal do Aluno</p>

                        <form action="{{ route('aluno.login') }}" method="POST">
                            {!! csrf_field() !!}
                            <label>
                                <span>login (e-mail):</span>
                                <input type="email" name="email" value="{{ old('email') }}" required>
                            </label>
                            <label>
                                <span>senha:</span>
                                <input type="password" name="password" required>
                            </label>
                            <div>
                                <p>Se esqueceu sua senha acesse o Portal do Aluno</p>
                                <input type="submit" value="ENTRAR">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="right">
                    <img src="{{ asset('assets/img/layout/img-bannerHome.jpg') }}" alt="">
                    <div>
                        <h3>Sua porta de entrada com acesso rápido ao Mercado de Trabalho</h3>
                        <h4>Com o suporte da Sequencial</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="chamadas">
        <div class="center">
            <div>
                <img src="{{ asset('assets/img/layout/ico1-destaques.png') }}" alt="">
                <span>Veja os<br>destaques</span>
            </div>
            <div>
                <img src="{{ asset('assets/img/layout/ico2-busque.png') }}" alt="">
                <span>Busque<br>vagas</span>
            </div>
            <div>
                <img src="{{ asset('assets/img/layout/ico3-envie.png') }}" alt="">
                <span>Envie<br>currículos</span>
            </div>
        </div>
    </div>

@endsection

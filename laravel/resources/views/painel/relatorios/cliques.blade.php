@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Relatórios /</small> Cliques</h2>
    </legend>

    <form action="{{ route('painel.relatorios.cliques') }}" method="GET" style="margin-bottom:30px">
        <div class="row">
            <div class="form-group col-lg-12" style="display:flex;align-items:flex-end">
                <div style="flex:1">
                    <label>Curso</label>
                    {!! Form::select('curso', $cursos, request('curso'), ['placeholder' => 'Todos os cursos', 'class' => 'form-control', 'style' => 'flex:1']) !!}
                </div>
                <div style="flex:1;margin:0 10px">
                    <label>Título</label>
                    <input type="text" name="termo" class="form-control" value="{{ request('termo') }}">
                </div>
                <div>
                    <label>Data de início</label>
                    <input type="text" name="data_inicio" value="{{ request('data_inicio') }}" class="datepicker datepicker-no-default form-control">
                </div>
                <div style="margin:0 10px">
                    <label>Data final</label>
                    <input type="text" name="data_final" value="{{ request('data_final') }}" class="datepicker datepicker-no-default form-control">
                </div>
                <button class="btn btn-info">
                    <span class="glyphicon glyphicon-search" style="margin-right:10px;"></span>
                    Filtrar
                </button>
            </div>
        </div>
    </form>

    <div class="row">
        <div class="col-lg-8">
            @if(!count($cliques))
            <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
            @else
            <table class="table table-striped table-bordered table-hover table-info">
                <thead>
                    <tr>
                        <th>Vaga</th>
                        <th>Cliques</th>
                    </tr>
                </thead>

                <tbody>
                @foreach ($cliques as $clique)
                    <tr class="tr-row">
                        <td>
                            <a href="{{ route('painel.vagas.edit', $clique->vaga->id) }}">
                                {{ $clique->vaga->titulo }}
                            </a>
                        </td>
                        <td>{{ $clique->total }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {!! $cliques->appends($_GET)->render() !!}
            @endif
        </div>
    </div>

@endsection

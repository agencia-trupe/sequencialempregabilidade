@extends('painel.common.template')

@section('content')

    <a href="{{ route('painel.relatorios.preenchidas') }}" class="btn btn-sm btn-default">
        &larr; Voltar
    </a>

    <legend>
        <h2><small>Relatórios / Vagas preenchidas /</small> {{ Tools::vagaStatus()[request('por')] }}</h2>
    </legend>

    <div class="row">
        <div class="col-lg-8">
            @if(!count($vagas))
            <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
            @else
            <table class="table table-striped table-bordered table-hover table-info">
                <thead>
                    <tr>
                        <th>Vaga</th>
                        <th>Preenchida em</th>
                        <th>Preenchida por</th>
                    </tr>
                </thead>

                <tbody>
                @foreach ($vagas as $vaga)
                    <tr class="tr-row">
                        <td>
                            <a href="{{ route('painel.vagas.edit', $vaga->id) }}">
                                {{ $vaga->titulo }}
                            </a>
                        </td>
                        <td>{{ $vaga->preenchida_em->format('d/m/Y H:i') }}</td>
                        <td>
                            <a href="{{ route('painel.usuarios.edit', $vaga->preenchidaPor->id) }}">
                                {{ $vaga->preenchidaPor->name }}
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {!! $vagas->appends($_GET)->render() !!}
            @endif
        </div>
    </div>

@endsection

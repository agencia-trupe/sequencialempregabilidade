@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Relatórios /</small> Vagas Preenchidas</h2>
    </legend>

    <div class="row" style="margin-top:30px">
        @foreach(Tools::vagaStatus() as $key => $label)
        <div class="col-md-4">
            <a href="{{ route('painel.relatorios.preenchidas', ['por' => $key]) }}" class="panel panel-default">
                <div class="panel-heading">{{ $label }}</div>
                <div class="panel-body">
                    <h1 style="margin:0;display:inline">{{ $contagem->has($key) ? $contagem[$key] : 0 }}</h1>
                    <h4 style="display:inline-block;margin-left:5px">vagas</h4>
                </div>
            </a>
        </div>
        @endforeach
    </div>

    <style>
        .panel { display: block; color: inherit; }
        .panel:hover { text-decoration: none; }
    </style>

@endsection

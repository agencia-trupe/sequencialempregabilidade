@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Relatórios /</small> Total de vagas</h2>
    </legend>

    <form action="{{ route('painel.relatorios.vagas') }}">
        <div class="row">
            <div class="form-group col-lg-5 col-md-6" style="display:flex">
                {!! Form::select('curso', $cursos, request('curso'), ['placeholder' => 'Todos os cursos', 'class' => 'form-control', 'style' => 'flex:1']) !!}

                <button class="btn btn-info btn-md" style="margin-left:10px">
                    <span class="glyphicon glyphicon-search" style="margin-right:10px"></span>
                    Filtrar
                </button>
            </div>
        </div>
    </form>

    <div class="row" style="margin-top:30px">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Total de vagas</div>
                <div class="panel-body">
                    <h1 style="margin:0;display:inline">{{ $total }}</h1>
                    <h4 style="display:inline-block;margin-left:5px">vagas</h4>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Vagas ativas</div>
                <div class="panel-body">
                    <h1 style="margin:0;display:inline">{{ $ativas }}</h1>
                    <h4 style="display:inline-block;margin-left:5px">vagas ativas</h4>
                </div>
            </div>
        </div>
    </div>

@endsection

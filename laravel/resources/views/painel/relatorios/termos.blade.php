@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Relatórios /</small> Termos buscados</h2>
    </legend>

    <form action="{{ route('painel.relatorios.termos') }}" method="GET" style="margin-bottom:30px">
        <div class="row">
            <div class="form-group col-lg-5 col-md-6" style="display:flex;align-items:flex-end">
                <div>
                    <label>Data de início</label>
                    <input type="text" name="data_inicio" value="{{ request('data_inicio') }}" class="datepicker datepicker-no-default form-control">
                </div>
                <div style="margin:0 10px">
                    <label>Data final</label>
                    <input type="text" name="data_final" value="{{ request('data_final') }}" class="datepicker datepicker-no-default form-control">
                </div>
                <button class="btn btn-info">
                    <span class="glyphicon glyphicon-search" style="margin-right:10px;"></span>
                    Filtrar
                </button>
            </div>
        </div>
    </form>

    <div class="row">
        <div class="col-lg-8">
            @if(!count($termos))
            <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
            @else
            <table class="table table-striped table-bordered table-hover table-info">
                <thead>
                    <tr>
                        <th>Termo</th>
                        <th>Buscas</th>
                    </tr>
                </thead>

                <tbody>
                @foreach ($termos as $termo)
                    <tr class="tr-row">
                        <td>{{ $termo->termo }}</td>
                        <td>{{ $termo->total }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {!! $termos->appends($_GET)->render() !!}
            @endif
        </div>
    </div>

@endsection

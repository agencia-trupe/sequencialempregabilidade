@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Vagas
            <div class="btn-group pull-right">
                <a href="{{ route('painel.vagas.cursos.index') }}" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-tag" style="margin-right:10px;"></span>Editar Cursos</a>
                <a href="{{ route('painel.vagas.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Vaga</a>
            </div>
        </h2>
    </legend>


    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Data de Cadastro</th>
                <th>Título</th>
                <th>Empresa</th>
                <th>Em exibição</th>
                <th>Preenchida</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>{{ $registro->data_cadastro }}</td>
                <td>{{ $registro->titulo }}</td>
                <td>{{ $registro->empresa }}</td>
                <td><span class="glyphicon glyphicon-{{ $registro->em_exibicao ? 'ok' : 'remove' }}"></span></td>
                <td>
                    @if($registro->preenchida)
                        {{ Tools::vagaStatus()[$registro->preenchida] }}
                    @else
                        Não
                    @endif
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.vagas.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.vagas.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {!! $registros->render() !!}
    @endif

@endsection

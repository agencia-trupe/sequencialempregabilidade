@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Vagas /</small> Adicionar Vaga</h2>
    </legend>

    {!! Form::open(['route' => 'painel.vagas.store', 'files' => true]) !!}

        @include('painel.vagas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection

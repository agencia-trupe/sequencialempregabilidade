@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.vagas.index') }}" title="Voltar para Vagas" class="btn btn-sm btn-default">
        &larr; Voltar para Vagas    </a>

    <legend>
        <h2>
            <small>Vagas /</small> Cursos

            <a href="{{ route('painel.vagas.cursos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Curso</a>
        </h2>
    </legend>

    @if(!count($cursos))
    <div class="alert alert-warning" role="alert">Nenhum curso cadastrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="vagas_cursos">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Título</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($cursos as $curso)
            <tr class="tr-row" id="{{ $curso->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>{{ $curso->titulo }}</td>
                <td class="crud-actions">
                    {!! Form::open(array('route' => array('painel.vagas.cursos.destroy', $curso->id), 'method' => 'delete')) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.vagas.cursos.edit', $curso->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection

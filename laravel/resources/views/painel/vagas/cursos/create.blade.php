@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Vagas /</small> Adicionar Curso</h2>
    </legend>

    {!! Form::open(['route' => 'painel.vagas.cursos.store']) !!}

        @include('painel.vagas.cursos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection

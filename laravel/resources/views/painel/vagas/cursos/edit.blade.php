@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Vagas /</small> Editar Curso</h2>
    </legend>

    {!! Form::model($curso, [
        'route'  => ['painel.vagas.cursos.update', $curso->id],
        'method' => 'patch'])
    !!}

    @include('painel.vagas.cursos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection

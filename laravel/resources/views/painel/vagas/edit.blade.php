@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Vagas /</small> Editar Vaga</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.vagas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.vagas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection

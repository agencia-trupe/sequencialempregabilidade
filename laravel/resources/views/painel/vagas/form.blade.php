@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('data_de_cadastro', 'Data de Cadastro') !!}
    {!! Form::text('data_de_cadastro', isset($registro) ? $registro->data_cadastro : null, ['class' => 'form-control datepicker auto-fill']) !!}
</div>

@if(isset($registro))
<div class="well">
    <p style="margin:0">
        Cadastrada por:
        <a href="{{ route('painel.usuarios.edit', $registro->cadastradaPor->id) }}"><strong>{{ $registro->cadastradaPor->name }}</strong></a>
        em
        <em>{{ $registro->cadastrada_em->format('d/m/y H:i') }}</em>
    </p>
</div>
@endif

<div class="form-group">
    {!! Form::label('cursos', 'Cursos') !!}
    {!! Form::select('cursos[]', $cursos, isset($registro) ? $registro->cursos->pluck('id')->toArray() : null, ['class' => 'form-control multi-select', 'multiple' => true]) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao', 'Descrição') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'descricao']) !!}
</div>

<div class="well">
    <div class="form-group">
        {!! Form::label('empresa', 'Empresa') !!}
        {!! Form::text('empresa', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('empresa_dados', 'Dados da Empresa (para uso interno)') !!}
        {!! Form::textarea('empresa_dados', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
    </div>

    <div class="form-group">
        <div class="checkbox" style="margin:0">
            <label>
                {!! Form::hidden('empresa_amiga', 0) !!}
                {!! Form::checkbox('empresa_amiga', 1) !!}
                <span style="font-weight:bold">Empresa Amiga</span>
            </label>
        </div>
    </div>

    <div class="form-group" style="margin:0">
        <div class="checkbox" style="margin:0">
            <label>
                {!! Form::hidden('ocultar_empresa', 0) !!}
                {!! Form::checkbox('ocultar_empresa', 1) !!}
                <span style="font-weight:bold">Ocultar empresa da vaga</span>
            </label>
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('local', 'Local') !!}
    {!! Form::text('local', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('horario', 'Horário') !!}
    {!! Form::text('horario', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('salario', 'Salário') !!}
    {!! Form::text('salario', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('beneficios', 'Benefícios') !!}
    {!! Form::text('beneficios', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="well">
    @foreach(\App\Models\Vaga::niveis() as $nivel)
    <div class="checkbox" style="margin:.5em 0 0">
        <label>
            <input type="checkbox" name="nivel[]" value="{{ $nivel }}" @if(isset($registro) && str_contains($registro->nivel, $nivel) || (count(old('nivel')) && in_array($nivel, old('nivel')))) checked @endif>
            <span style="font-weight:bold">{{ ucfirst($nivel) }}</span>
        </label>
    </div>
    @endforeach
</div>

<div class="well">
    <div class="form-group" style="margin:0">
        <div class="checkbox" style="margin:0">
            <label>
                {!! Form::hidden('ativa', 0) !!}
                {!! Form::checkbox('ativa', 1) !!}
                <span style="font-weight:bold">Vaga Ativa</span>
            </label>
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('data_de_expiracao', 'Data de Expiração (opcional)') !!}
    {!! Form::text('data_de_expiracao', isset($registro) ? $registro->data_formatada : null, ['class' => 'form-control datepicker']) !!}
</div>

<hr>

<p class="alert alert-info small" style="margin-bottom: 15px; height:45px; padding: 12px 15px;">
    <span class="glyphicon glyphicon-info-sign" style="margin-right:10px;"></span>
    Preencha ao menos um dos campos abaixo.
</p>

<div class="form-group">
    {!! Form::label('telefone', 'Telefone') !!}
    {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('e_mail', 'E-mail') !!}
    {!! Form::text('e_mail', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('site', 'Site') !!}
    {!! Form::text('site', null, ['class' => 'form-control']) !!}
</div>

@if(isset($registro))
<div class="well">
    <div class="form-group" style="margin:0">
        {!! Form::label('preenchida', 'Vaga Preenchida') !!}
        {!! Form::select('preenchida', Tools::vagaStatus(), null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
    </div>
</div>
@endif

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.vagas.index') }}" class="btn btn-default btn-voltar">Voltar</a>

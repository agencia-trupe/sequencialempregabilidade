<ul class="nav navbar-nav">
	<li @if(Tools::routeIs('painel.vagas*')) class="active" @endif>
		<a href="{{ route('painel.vagas.index') }}">Vagas</a>
    </li>
    <li class="dropdown @if(Tools::routeIs('painel.relatorios*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Relatórios
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.relatorios.vagas')) class="active" @endif>
                <a href="{{ route('painel.relatorios.vagas') }}">Total de vagas</a>
            </li>
            <li @if(Tools::routeIs('painel.relatorios.cliques')) class="active" @endif>
                <a href="{{ route('painel.relatorios.cliques') }}">Cliques</a>
            </li>
            <li @if(Tools::routeIs('painel.relatorios.termos')) class="active" @endif>
                <a href="{{ route('painel.relatorios.termos') }}">Termos buscados</a>
            </li>
            <li @if(Tools::routeIs('painel.relatorios.cursos')) class="active" @endif>
                <a href="{{ route('painel.relatorios.cursos') }}">Cursos buscados</a>
            </li>
            <li @if(Tools::routeIs('painel.relatorios.preenchidas')) class="active" @endif>
                <a href="{{ route('painel.relatorios.preenchidas') }}">Vagas preenchidas</a>
            </li>
        </ul>
	</li>
    {{-- <li class="dropdown @if(Tools::routeIs('painel.contato*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
                <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
            </li>
            <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>
                <a href="{{ route('painel.contato.recebidos.index') }}">
                    Contatos Recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li> --}}
</ul>
